# ELK

The ELK Stack, now known as the Elastic Stack, is a powerful set of open-source tools developed by Elastic for ingesting, storing, searching, analyzing, and visualizing large volumes of data. The Elastic Stack consists of three main components: Elasticsearch, Logstash, and Kibana, along with other optional components like Beats and APM Server.

## Installation
```
https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html

After configuring compose file we edit linux memlimit on docker host as root

sudo echo "vm.max_map_count=262144" >> /etc/sysctl.conf

```
## Environment

elastic search : elasticsearch:9200

kibana : http://52.78.204.252:5601

Username : elastic / admin@123